package com.ing4isi.tparhcsi.Gestion_Composite.Service;

import com.ing4isi.tparhcsi.Gestion_Composite.Dto.Candidat;
import com.ing4isi.tparhcsi.Gestion_Composite.Dto.Etudiant;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

@Service
public class CompositeService {

    public RestTemplate restTemplate = new RestTemplate();

    public ResponseEntity<List<Etudiant>> callGetAllEtudiants() {
        String url = "http://localhost:8071/Gestion_Inscriptions/Etudiants";
        ParameterizedTypeReference<List<Etudiant>> responseType = new ParameterizedTypeReference<List<Etudiant>>() {};
        ResponseEntity<List<Etudiant>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);
        System.out.println(responseEntity);
        return responseEntity;
    }

    public ResponseEntity<Etudiant> callGetEtudiant(int id){
        String url = "http://localhost:8071/Gestion_Inscriptions/Etudiant/"+id;
        ParameterizedTypeReference<Etudiant> responseType = new ParameterizedTypeReference<Etudiant>() {};
        ResponseEntity<Etudiant> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);
        System.out.println(responseEntity);
        return responseEntity;
    }

    public ResponseEntity<String> callDeleteEtudiant(int id, boolean statut) {
        String url = "http://localhost:8071/Gestion_Inscriptions/delEtudiant/"+id+"/"+statut;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, null, String.class);
        System.out.println("reponse = " +response.getBody());
        return response;
    }

    public ResponseEntity<String> callModifierEtudiant(Etudiant etudiant){
        String url = "http://localhost:8071/Gestion_Inscriptions/upEtudiant";
        HttpEntity<Etudiant> httpEntity = new HttpEntity<>(etudiant);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class );
        System.out.println("reponse = " +response.getBody());
        return response;
    }

    public ResponseEntity<String> callCreerEtudiant(Etudiant etudiant){
        String urlCreation = "http://localhost:8071/Gestion_Inscriptions/createEtudiant";
        String urlVerification = "http://localhost:8070/Gestion_Etudiants/Candidat/"+etudiant.getId();
        ResponseEntity<Candidat> verif = restTemplate.exchange(urlVerification, HttpMethod.GET, null, Candidat.class);

        if(verif.getBody() != null){
            String UrlEmail = "http://localhost:8074/Gestion_Email/envoisMail/"+verif.getBody().getMail();
            HttpEntity<Etudiant> httpEntity = new HttpEntity<>(etudiant);
            ResponseEntity<String> response = restTemplate.exchange(urlCreation, HttpMethod.POST, httpEntity, String.class );
            ResponseEntity<String> reponseEmail = restTemplate.exchange(UrlEmail, HttpMethod.POST, null, String.class);
            System.out.println("reponse = " +response.getBody());
            return response;
        }else
            return new ResponseEntity<>("not good in Composite ", HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<List<Candidat>> callGetAllCandidats(){
        String url = "http://localhost:8070/Gestion_Etudiants/listAllCandidats";
        ParameterizedTypeReference<List<Candidat>> responseType = new ParameterizedTypeReference<List<Candidat>>() {};
        ResponseEntity<List<Candidat>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);
        System.out.println(responseEntity);
        return responseEntity;
    }

    public ResponseEntity<Candidat> callGetCandidat(long id){
        String url = "http://localhost:8070/Gestion_Etudiants/Candidat/"+id;
        ParameterizedTypeReference<Candidat> responseType = new ParameterizedTypeReference<Candidat>() {};
        ResponseEntity<Candidat> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, responseType);
        System.out.println(responseEntity);
        return responseEntity;
    }

    public ResponseEntity<String> callCreateCandidat(Candidat candidat) {
        String url = "http://localhost:8070/Gestion_Etudiants/createCandidat";
        HttpEntity<Candidat> httpEntity = new HttpEntity<>(candidat);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class );
        System.out.println("reponse = " +response.getBody());
        return response;

    }

    public ResponseEntity<String> callUpdateCandidat(Candidat candidat){
        String url = "http://localhost:8070/Gestion_Etudiants/modifCandidat";
        HttpEntity<Candidat> httpEntity = new HttpEntity<>(candidat);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class );
        System.out.println("reponse = " +response.getBody());
        return response;

    }

    public ResponseEntity<String> callChangeStatutCandidat(long id, boolean statut){
        String url = "http://localhost:8070/Gestion_Etudiants/changeStatutCandidat/"+id+"/"+statut;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, null, String.class );
        System.out.println("reponse = " +response.getBody());
        return response;

    }

    public ResponseEntity<List<Candidat>> getListeEtudiantInscrits(){
        String urlEtudiants = "http://localhost:8071/Gestion_Inscriptions/Etudiants";
        ParameterizedTypeReference<Etudiant[]> responseType = new ParameterizedTypeReference<Etudiant[]>() {};
        ResponseEntity<Etudiant[]> responseEntity = restTemplate.exchange(urlEtudiants, HttpMethod.GET, null, responseType);

        List<Long> ids = new ArrayList<>();
        Etudiant[] etudiants = responseEntity.getBody();

        for(Etudiant etudiant : etudiants){
            ids.add(etudiant.getId());
        }

        String urlCandidats = "http://localhost:8070/Gestion_Etudiants/listAllCandidatsByIds";

        HttpEntity<List<Long>> httpEntity = new HttpEntity<>(ids);
        ParameterizedTypeReference<List<Candidat>> responseTypeCandidat = new ParameterizedTypeReference<List<Candidat>>() {};

        //ResponseEntity<List<Candidat>> response = restTemplate.exchange(urlCandidats, HttpMethod.GET, httpEntity, responseTypeCandidat );

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlCandidats)
                .queryParam("ids", ids.toArray());


        ResponseEntity<List<Candidat>> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, responseTypeCandidat);


        return response;

    }



}
