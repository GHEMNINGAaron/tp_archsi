package com.ing4isi.tparhcsi.Gestion_Composite.Dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Candidat {


    private long id;

    private boolean statut;

    private String nom;

    private  String prenom;

    private String filiere;

    private int niveau;

    private Date dateNaissance;

    private Date date_creation;

    private String mail;

    private String login;

    private String password;

}
