package com.ing4isi.tparhcsi.Gestion_Composite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class GestionCompositeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionCompositeApplication.class, args);
	}

}
