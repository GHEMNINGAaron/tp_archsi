package com.ing4isi.tparhcsi.Gestion_Composite.Dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonSerialize
@JsonDeserialize
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Etudiant {

    public enum StatutPaiement{termine, en_cours}

    @JsonProperty("id")
    private long id;

    @JsonProperty("pension")
    private long pension;

    @JsonProperty("statutdePaiement")
    private StatutPaiement statutdePaiement ;

    @JsonProperty("statut")
    private boolean statut;
}
