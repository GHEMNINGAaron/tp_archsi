package com.ing4isi.tparhcsi.Gestion_Composite.Controller;

import com.ing4isi.tparhcsi.Gestion_Composite.Dto.Candidat;
import com.ing4isi.tparhcsi.Gestion_Composite.Dto.Etudiant;
import com.ing4isi.tparhcsi.Gestion_Composite.Service.CompositeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@RestController
@RequestMapping("/Gestion_Composite")
public class CompositeController {

    @Autowired
    public CompositeService compositeService;

    @GetMapping("/listAllCandidats")
    public ResponseEntity<List<Candidat>> getAllCandidats() {
        return compositeService.callGetAllCandidats();
    }

    @GetMapping("/listAllEtudiants")
    public ResponseEntity<List<Etudiant>> getAllEtudiants() {
        return compositeService.callGetAllEtudiants();
    }

    @GetMapping("/listAllCandidatsInscrits")
    public  ResponseEntity<List<Candidat>> getAllCandidatsInscrits(){
        return compositeService.getListeEtudiantInscrits();
    }

    @GetMapping("/Candidat/{id}")
    public ResponseEntity<Candidat> getCandidat(@PathVariable("id") int id) {
        System.out.println("id = "+id);
        return compositeService.callGetCandidat(id);
    }

    @GetMapping("/Etudiant/{id}")
    public ResponseEntity<Etudiant> getEtudiant(@PathVariable("id") int id){
        return compositeService.callGetEtudiant(id);
    }

    @PostMapping("/createCandidat")
    public ResponseEntity<String> createCandidat(@RequestBody Candidat candidat){
        return compositeService.callCreateCandidat(candidat);
    }

    @PostMapping("/createEtudiant")
    public ResponseEntity<String> createEtudiant(@RequestBody Etudiant etudiant){
        return compositeService.callCreerEtudiant(etudiant);
    }

    @PutMapping("/delEtudiant/{id}/{statut}")
    public ResponseEntity<String> deleteEtudiant(@PathVariable("id") int id, @PathVariable("statut") boolean statut) {
        return compositeService.callDeleteEtudiant(id, statut);
    }

    @PutMapping("/changeStatutCandidat/{id}/{statut}")
    public ResponseEntity<String> changeStatutCandidat(@PathVariable("id") Long id, @PathVariable("statut") boolean statut){
        return compositeService.callChangeStatutCandidat(id, statut);
    }

    @PutMapping("/upEtudiant")
    public ResponseEntity<String> modifierEtudiant(@RequestBody Etudiant etudiant){
        return  compositeService.callModifierEtudiant(etudiant);
    }

    @PutMapping("/modifCandidat")
    public ResponseEntity<String> updateCandidat(@RequestBody Candidat candidat){
        return compositeService.callUpdateCandidat(candidat);
    }

}
