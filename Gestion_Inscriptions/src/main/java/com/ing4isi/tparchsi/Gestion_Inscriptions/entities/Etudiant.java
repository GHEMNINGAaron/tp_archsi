package com.ing4isi.tparchsi.Gestion_Inscriptions.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "etudiant")
public class Etudiant {

    public enum StatutPaiement{termine, en_cours}

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "pension")
    private long pension;

    @Column(name = "statutdePaiement")
    @Enumerated(EnumType.STRING)
    private StatutPaiement statutdePaiement ;

    @Column(name = "statut")
    private boolean statut;
}
