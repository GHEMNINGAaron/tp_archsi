package com.ing4isi.tparchsi.Gestion_Inscriptions.repositories;

import com.ing4isi.tparchsi.Gestion_Inscriptions.entities.Etudiant;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {

    Etudiant findById(long id);
}
