package com.ing4isi.tparchsi.Gestion_Inscriptions.controller;

import com.ing4isi.tparchsi.Gestion_Inscriptions.entities.Etudiant;
import com.ing4isi.tparchsi.Gestion_Inscriptions.services.EtudiantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Gestion_Inscriptions")
public class EtudiantController {

    @Autowired
    EtudiantService etudiantService;

    @GetMapping("/Etudiants")
    public ResponseEntity<List<Etudiant>> getAllEtudiants() {
        return new ResponseEntity<>(etudiantService.getAllEtudiants(), HttpStatus.OK);
    }

    @GetMapping("/Etudiant/{id}")
    public ResponseEntity<Etudiant> getEtudiant(@PathVariable("id") int id){
        return new ResponseEntity<>(etudiantService.getEtudiant(id), HttpStatus.OK);
    }

    @PutMapping("/delEtudiant/{id}/{statut}")
    public ResponseEntity<String> deleteEtudiant(@PathVariable("id") int id, @PathVariable("statut") boolean statut){
        try {
            etudiantService.deleteEtudiant(id, statut);
            System.out.println("Suppression réussie");
        }catch (Exception e ){
            System.out.println("erreur : "+e);
            return new ResponseEntity<>("Not Good", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Good", HttpStatus.OK);
    }

    @PutMapping("/upEtudiant")
    public ResponseEntity<String> modifierEtudiant(@RequestBody Etudiant etudiant){

        try {
            etudiantService.updateEtudiant(etudiant);
            System.out.println("Update réussie");
        }catch (Exception e ){
            System.out.println("erreur : "+e);
            return new ResponseEntity<>("Not Good", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Good", HttpStatus.OK);
    }

    @PostMapping("/createEtudiant")
    public ResponseEntity<String> creerEtudiant(@RequestBody Etudiant etudiant){

        try {
            etudiantService.createEtudiant(etudiant);
            System.out.println("Création réussie");
        }catch (Exception e ){
            System.out.println("erreur : "+e);
            return new ResponseEntity<>("Not Good", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Good", HttpStatus.CREATED);
    }


}
