package com.ing4isi.tparchsi.Gestion_Inscriptions.services;

import com.ing4isi.tparchsi.Gestion_Inscriptions.entities.Etudiant;
import com.ing4isi.tparchsi.Gestion_Inscriptions.repositories.EtudiantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EtudiantService {

    @Autowired
    private EtudiantRepository etudiantRepository;

    public Etudiant getEtudiant(long id) {
        return etudiantRepository.findById(id);
    }

    public List<Etudiant> getAllEtudiants() {
        return  etudiantRepository.findAll();
    }

    public void createEtudiant(Etudiant etudiantData) {
        try{
            Etudiant etudiant = etudiantData;
            if(etudiant.getPension()==1600000)
                etudiant.setStatutdePaiement(Etudiant.StatutPaiement.termine);
            else
                etudiant.setStatutdePaiement(Etudiant.StatutPaiement.en_cours);
            etudiantRepository.save(etudiant);
            System.out.println("Etudiant créé");
        }catch (Exception e) {
            System.out.println("error = " + e);
        }
    }

    public void deleteEtudiant(long id, boolean statut){
        try{
            Etudiant etudiant = etudiantRepository.findById(id);
            etudiant.setStatut(statut);
            etudiantRepository.save(etudiant);
            System.out.println("Etudiant sauvegardé");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateEtudiant(Etudiant etudiantData){
        try{
            Etudiant etudiant = etudiantRepository.findById(etudiantData.getId());
            etudiant.setPension(etudiantData.getPension());
            if(etudiant.getPension()==1600000)
                etudiant.setStatutdePaiement(Etudiant.StatutPaiement.termine);
            else
                etudiant.setStatutdePaiement(Etudiant.StatutPaiement.en_cours);

            etudiantRepository.save(etudiant);

            System.out.println("Etudiant update avec succès");
        }catch (Exception e) {
            System.out.println("Erreur update = "+e);
        }
    }
}
