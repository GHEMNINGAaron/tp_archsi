package com.ing4isi.tparchsi.Gestion_Notes.repository;

import com.ing4isi.tparchsi.Gestion_Notes.entites.Note;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Long> {
    List<Note> findByEtudiantId(Long etudiantId);


    List<Note> findByMatiereId(Long matiereId);
}
