package com.ing4isi.tparchsi.Gestion_Notes.service;

import com.ing4isi.tparchsi.Gestion_Notes.entites.Matiere;
import com.ing4isi.tparchsi.Gestion_Notes.entites.Note;
import com.ing4isi.tparchsi.Gestion_Notes.repository.MatiereRepository;
import com.ing4isi.tparchsi.Gestion_Notes.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private MatiereRepository matiereRepository;

    public Note enregistrerNote(Note note) {
        return noteRepository.save(note);
    }

    public List<Note> listerNotesEtudiant(Long etudiantId) {
        return noteRepository.findByEtudiantId(etudiantId);
    }

    public List<Note> listerNotesMatiere(Long matiereId) {
        return noteRepository.findByMatiereId(matiereId);
    }


    public double calculerMoyenneEtudiant(Long etudiantId) {
        List<Note> notesEtudiant = listerNotesEtudiant(etudiantId);

        if (notesEtudiant.isEmpty()) {
            return 0.0; // Aucune note pour cet étudiant
        }

        double sommeNotesPonderees = notesEtudiant.stream()
                .mapToDouble(note -> {
                    Matiere matiere = matiereRepository.findById(note.getMatiereId()).orElse(null);
                    return matiere != null ? note.getValeur() * matiere.getCredits() : 0;
                })
                .sum();

        int totalCredits = notesEtudiant.stream()
                .map(note -> matiereRepository.findById(note.getMatiereId()).map(Matiere::getCredits).orElse(0))
                .mapToInt(Integer::intValue)
                .sum();

        return sommeNotesPonderees / totalCredits;
    }


}
