package com.ing4isi.tparchsi.Gestion_Notes.entites;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonSerialize
@JsonDeserialize
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Etudiant {

    public enum StatutPaiement{termine, en_cours}

    @JsonProperty("id")
    private long id;

    @JsonProperty("pension")
    private long pension;

    @JsonProperty("statutdePaiement")
    private StatutPaiement statutdePaiement ;

    @JsonProperty("statut")
    private boolean statut;
}
