package com.ing4isi.tparchsi.Gestion_Notes.repository;

import com.ing4isi.tparchsi.Gestion_Notes.entites.Matiere;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MatiereRepository extends JpaRepository<Matiere, Long> {

        Optional<Matiere> findById(long matiereId);

    }

