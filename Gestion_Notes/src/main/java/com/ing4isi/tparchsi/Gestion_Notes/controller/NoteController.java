package com.ing4isi.tparchsi.Gestion_Notes.controller;

import com.ing4isi.tparchsi.Gestion_Notes.entites.Etudiant;
import com.ing4isi.tparchsi.Gestion_Notes.entites.Matiere;
import com.ing4isi.tparchsi.Gestion_Notes.entites.Note;
import com.ing4isi.tparchsi.Gestion_Notes.service.MatiereService;
import com.ing4isi.tparchsi.Gestion_Notes.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;



@RestController
@RequestMapping("/gestion_notes")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @Autowired
    private MatiereService matiereService;

    public RestTemplate restTemplate = new RestTemplate();

    @PostMapping("/createNote")
    public ResponseEntity<String> enregistrerNote(@RequestBody Note note) {

        String urlEtudiant = "http://localhost:8071/Gestion_Inscriptions/Etudiant/"+note.getEtudiantId();

        ResponseEntity<Etudiant> verif = restTemplate.exchange(urlEtudiant, HttpMethod.GET, null, Etudiant.class);
        if(verif.getBody() != null){
            System.out.println("Etudiant :"+verif.getBody());
            Matiere matiere = matiereService.getMatiere(note.getMatiereId());
            if(matiere != null){
                try{
                    noteService.enregistrerNote(note);
                } catch (Exception e) {
                    System.out.println("Erreur : "+e);
                    return new ResponseEntity<>("Not Good", HttpStatus.BAD_REQUEST);
                }
                return new ResponseEntity<>("Good", HttpStatus.OK);
            }
            else {
                System.out.println("Matiere absente de la bd");
                return new ResponseEntity<>("Not Good", HttpStatus.BAD_REQUEST);
            }
        }
        else {
            System.out.println("Etudiant absent de la bd");
            return new ResponseEntity<>("Not Good", HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/NotesEtudiant/{etudiantId}")
    public ResponseEntity<List<Note>> listerNotesEtudiant(@PathVariable Long etudiantId) {
        return new ResponseEntity<>(noteService.listerNotesEtudiant(etudiantId), HttpStatus.OK);
    }

    @GetMapping("/MoyenneEtudiant/{etudiantId}")
    public ResponseEntity<Double> calculerMoyenneEtudiant(@PathVariable Long etudiantId) {
        return new ResponseEntity<>(noteService.calculerMoyenneEtudiant(etudiantId), HttpStatus.OK);
    }


    @GetMapping("/NotesMatiere/{matiereId}")
    public ResponseEntity<List<Note>> listerNotesMatiere(@PathVariable Long matiereId) {
        return new ResponseEntity<>(noteService.listerNotesMatiere(matiereId), HttpStatus.OK);
    }
}
