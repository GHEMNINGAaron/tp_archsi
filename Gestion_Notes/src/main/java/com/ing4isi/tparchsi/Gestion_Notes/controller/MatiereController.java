package com.ing4isi.tparchsi.Gestion_Notes.controller;

import com.ing4isi.tparchsi.Gestion_Notes.entites.Matiere;
import com.ing4isi.tparchsi.Gestion_Notes.service.MatiereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gestion_notes")
public class MatiereController {

    @Autowired
    private MatiereService matiereService;

    @PostMapping("/createMatiere")
    public ResponseEntity<String> enregistrerMatiere(@RequestBody Matiere matiere) {
        try{
            matiereService.enregistrerMatiere(matiere);
        } catch (Exception e){
            System.out.println("Erreur : "+e);
            return  new ResponseEntity<>("Not Good ", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Good", HttpStatus.CREATED);

    }
    //@GetMapping("/etudiant/{etudiantId}")
   // public List<Note> listerMatiere(@PathVariable Matiere matiere) {
        //return matiereService.enregistrerMatiere( matiere);
    //}

    @GetMapping("/listeMatiere")
    public ResponseEntity<List<Matiere>> getListMatieres(){
        return new ResponseEntity<>(matiereService.getListMatiere(), HttpStatus.OK);
    }

    @GetMapping("/Matiere/{id}")
    public ResponseEntity<Matiere> getMatieres(@PathVariable("id") Long id){
        return new ResponseEntity<>(matiereService.getMatiere(id), HttpStatus.OK);
    }
}

