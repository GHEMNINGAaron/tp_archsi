package com.ing4isi.tparchsi.Gestion_Notes.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "matiere")
public class Matiere {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long matiereid;

    private String nom;
    private int credits; // Ajout de la propriété "credits"

}
