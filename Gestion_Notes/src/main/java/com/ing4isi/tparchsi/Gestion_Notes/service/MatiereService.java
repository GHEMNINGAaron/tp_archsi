package com.ing4isi.tparchsi.Gestion_Notes.service;

import com.ing4isi.tparchsi.Gestion_Notes.entites.Matiere;
import com.ing4isi.tparchsi.Gestion_Notes.repository.MatiereRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatiereService {

    @Autowired
    private MatiereRepository matiereRepository;

    public Matiere enregistrerMatiere(Matiere matiere) {
        return matiereRepository.save(matiere);
    }

    public List<Matiere> getListMatiere(){
        return matiereRepository.findAll();
    }

    public Matiere getMatiere(Long id){
        return matiereRepository.findById(id).orElse(null);
    }

}
