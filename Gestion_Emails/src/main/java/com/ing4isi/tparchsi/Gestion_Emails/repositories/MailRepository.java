package com.ing4isi.tparchsi.Gestion_Emails.repositories;


import com.ing4isi.tparchsi.Gestion_Emails.entities.Mail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailRepository extends JpaRepository<Mail, Long> {
}
