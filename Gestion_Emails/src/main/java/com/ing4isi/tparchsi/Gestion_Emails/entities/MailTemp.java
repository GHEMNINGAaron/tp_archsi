package com.ing4isi.tparchsi.Gestion_Emails.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "mail_tempon")
public class MailTemp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idMail")
    private Long idMailTemp;

    private String destinateur;
    private String destinataire;
    private String subject;
    private String body;
    private int isSended;


}
