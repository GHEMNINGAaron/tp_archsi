package com.ing4isi.tparchsi.Gestion_Emails.services;


import com.ing4isi.tparchsi.Gestion_Emails.entities.Mail;
import com.ing4isi.tparchsi.Gestion_Emails.entities.MailTemp;
import com.ing4isi.tparchsi.Gestion_Emails.repositories.EmailTempRepository;
import com.ing4isi.tparchsi.Gestion_Emails.repositories.MailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.nio.file.Files.copy;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    MailRepository mailRepository;

    @Autowired
    EmailTempRepository emailTempRepository;



    private String email = "aaronkom345kata@gmail.com";

    //    Verifier la connexion a internet
    public boolean isConnectedToNetwork() {
        try {
            InetAddress address = InetAddress.getByName("www.google.com");
            return address.isReachable(5000); // 5000 ms timeout
        } catch (UnknownHostException e) {
            return false; // Unable to resolve host, so not connected
        } catch (Exception e) {
            return false; // Other exceptions, assume not connected
        }
    }

    public void sendEmail(String toEmail) {
        String subject = "Inscription de l'etudiant";
        String body ="L'étudiant a bien été inscit dans l'application.";
//        if (this.isConnectedToNetwork()) {
        try {
//                Envoie de l'email
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(this.email);
            message.setTo(toEmail);
            message.setText(body);
            message.setSubject(subject);

            mailSender.send(message);
            System.out.println("Mail sent successfully...");

//                Ajout de l'email en base de donnees avec le statut envoye
            Mail email = new Mail(null, null, this.email, toEmail, subject, body, 1);
            mailRepository.save(email);

        } catch (Exception e) {
//                Ajout de l'email en base de donnees avec le statut non envoye
            MailTemp email = new MailTemp(null, this.email, toEmail, subject, body, 0);
            emailTempRepository.save(email);
            System.out.println("Erreur : " + e);
        }
//        }
    }

}