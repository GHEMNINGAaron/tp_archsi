package com.ing4isi.tparchsi.Gestion_Emails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionEmailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionEmailsApplication.class, args);
	}

}
