package com.ing4isi.tparchsi.Gestion_Emails.repositories;

import com.ing4isi.tparchsi.Gestion_Emails.entities.MailTemp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailTempRepository extends JpaRepository<MailTemp, Long> {
}
