package com.ing4isi.tparchsi.Gestion_Emails.controller;

import com.ing4isi.tparchsi.Gestion_Emails.entities.Mail;
import com.ing4isi.tparchsi.Gestion_Emails.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Gestion_Email")
public class EmailController {

    @Autowired
    public EmailService emailService;

    @PostMapping("/envoisMail/{email}")
    public ResponseEntity<String> envoisMail(@PathVariable("email") String email){
        try{
            emailService.sendEmail(email);
        }catch (Exception e){
            System.out.println("Erreur envois de mail : "+e);
            return new ResponseEntity<>("Not Good", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Good", HttpStatus.OK);

    }
}
