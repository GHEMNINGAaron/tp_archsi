package com.ing4isi.tparchsi.Gestion_Etudiants.controller;

import com.ing4isi.tparchsi.Gestion_Etudiants.entities.Candidat;
import com.ing4isi.tparchsi.Gestion_Etudiants.services.CandidatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Gestion_Etudiants")
public class CandidatController {

    @Autowired
    public CandidatService candidatService;

    @GetMapping("/listAllCandidats")
    public ResponseEntity<List<Candidat>> getAllCandidats() {
        return new ResponseEntity<>(candidatService.getAllInscrits(), HttpStatus.OK);
    }

    @GetMapping("/listAllCandidatsByIds")
    public ResponseEntity<List<Candidat>> getAllCandidatsByIds(@RequestParam List<Long> ids) {
        return new ResponseEntity<>(candidatService.getAllInscritsByIds(ids), HttpStatus.OK);
    }

    @GetMapping("/Candidat/{id}")
    public ResponseEntity<Candidat> getCandidat(@PathVariable("id") long id) {
        System.out.println("id = "+id);
        return new ResponseEntity<>(candidatService.getInscrit(id), HttpStatus.OK);
    }

    @PostMapping("/createCandidat")
    public ResponseEntity<String> createCandidat(@RequestBody Candidat candidat){
        try{
            candidatService.createInscrit(candidat);
        } catch (Exception e){
            System.out.println("Erreur : "+e);
            return  new ResponseEntity<>("Not Good ", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Good", HttpStatus.CREATED);
    }

    @PutMapping("/modifCandidat")
    public ResponseEntity<String> updateCandidat(@RequestBody Candidat candidat){
        try{
            candidatService.updateCandidat(candidat);
        } catch (Exception e){
            System.out.println("Erreur : "+e);
            return  new ResponseEntity<>("Not Good ", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Good", HttpStatus.CREATED);
    }

    @PutMapping("/changeStatutCandidat/{id}/{statut}")
    public ResponseEntity<String> changeStatutCandidat(@PathVariable("id") Long id, @PathVariable("statut") boolean statut){
        try{
            candidatService.changeStatutInscrit(id, statut);
        } catch (Exception e){
            System.out.println("Erreur : "+e);
            return  new ResponseEntity<>("Not Good ", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Good", HttpStatus.CREATED);
    }


}
