package com.ing4isi.tparchsi.Gestion_Etudiants;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class GestionEtudiantsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionEtudiantsApplication.class, args);
	}

}
