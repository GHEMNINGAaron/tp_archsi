package com.ing4isi.tparchsi.Gestion_Etudiants.services;

import com.ing4isi.tparchsi.Gestion_Etudiants.entities.Candidat;
import com.ing4isi.tparchsi.Gestion_Etudiants.repositories.CandidatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidatService {
    
    @Autowired
    private CandidatRepository inscritRepository;

    public Candidat getInscrit(long id) {
        return inscritRepository.findById(id);
    }

    public List<Candidat> getAllInscrits() {
        return  inscritRepository.findAll();
    }

    public List<Candidat> getAllInscritsByIds(List<Long> ids){
        return  inscritRepository.findAllByIdIn(ids);
    }

    public void createInscrit(Candidat Inscrit) {
        try{
            inscritRepository.save(Inscrit);
            System.out.println("Inscrit créé");
        }catch (Exception e) {
            System.out.println("error = " + e);
        }
    }

    public void changeStatutInscrit(long id, boolean statut){
        try{
            Candidat inscrit = inscritRepository.findById(id);
            inscrit.setStatut(statut);
            inscritRepository.save(inscrit);
            System.out.println("Inscrit sauvegardé");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateCandidat(Candidat candidatData){
        try{
            Candidat candidat = inscritRepository.findById(candidatData.getId());
            if(candidat != null){
                candidat.setNom(candidatData.getNom());
                candidat.setPrenom(candidatData.getPrenom());
                candidat.setFiliere(candidatData.getFiliere());
                candidat.setNiveau(candidatData.getNiveau());
                candidat.setDateNaissance(candidatData.getDateNaissance());
                inscritRepository.save(candidat);
                System.out.println("Inscrit update avec succès");
            }
            else
                System.out.println("candidat vide");
        }catch (Exception e) {
            System.out.println("Erreur update = "+e);
        }
    }
}
