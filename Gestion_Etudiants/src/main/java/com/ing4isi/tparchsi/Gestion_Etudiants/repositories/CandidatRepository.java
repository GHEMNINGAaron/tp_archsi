package com.ing4isi.tparchsi.Gestion_Etudiants.repositories;

import com.ing4isi.tparchsi.Gestion_Etudiants.entities.Candidat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidatRepository extends JpaRepository<Candidat, Long> {

    //Candidat findById(Long id);
    Candidat findById(long id);

    List<Candidat> findAllByIdIn(List<Long> ids);
}
